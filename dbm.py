import time

def check_unhappy(g, k, price, number_of_users, number_of_arms):
	for i in range(number_of_users):
		max_net_reward = g[i][k[i]] - price[k[i]]
		for j in range(number_of_arms):
			if g[i][j] - price[j] > max_net_reward:
				return i
	return -1

def all_received_bids(price, number_of_arms):
	for i in range(number_of_arms):
		if price[i] == 0:
			return False

	return True

def find_best_arm(i, g, k, price, number_of_arms):
	max_net_reward = 0
	best_arm = k[i]
	for j in range(number_of_arms):
		if (g[i][j] - price[j] > max_net_reward):
			best_arm = j

	return best_arm

def find_second_best_arm(i, g, k, price, number_of_arms, best_arm):
	max_net_reward = 0
	second_best_arm  = k[i]
	for j in range(number_of_arms):
		if (j == best_arm):
			continue
		elif(g[i][j] - price[j] > max_net_reward):
			second_best_arm = j
	return second_best_arm


def owner(arm, number_of_users, k):
	for i in range(number_of_users):
		if (k[i] == arm):
			return i
	return -1


def exchange_arms(unhappy_user, best_arm, k, number_of_arms, number_of_users):
	owner_of_best_arm = owner(best_arm, number_of_users, k)

	if (owner_of_best_arm != -1):
		unhappy_user_arm = k[unhappy_user]
		k[unhappy_user] = best_arm
		k[owner_of_best_arm] = unhappy_user_arm
	else:
		k[unhappy_user] = best_arm



def DBM(g, prevk, number_of_users, number_of_arms):
	epsilon = 0.05
	k = []
	bid = 0
	price = [0 for i in range(number_of_arms)]
	for i in range(number_of_users):
		k.append(prevk[i])

	unhappy_user = check_unhappy(g, k, price, number_of_users, number_of_arms)

	while (unhappy_user != -1 and not all_received_bids(price, number_of_arms)):
		best_arm = find_best_arm(unhappy_user, g, k, price, number_of_arms)
		second_best_arm = find_second_best_arm(unhappy_user, g, k, price, number_of_arms, best_arm)
		exchange_arms(unhappy_user, best_arm, k, number_of_arms, number_of_users)
		bid = (int)(g[unhappy_user][best_arm] - price[best_arm]) - (int)(g[unhappy_user][second_best_arm] - price[second_best_arm]) + epsilon
		price[best_arm]  = price[best_arm] + bid
		unhappy_user = check_unhappy(g, k, price, number_of_users, number_of_arms)
		#print(k)

	return k

'''
g = [[(j+5)/10 for j in range(5)] for i in range(5)]
k = [i for i in range(5)]
number_of_users = 5
number_of_arms = 5

print('Distribution: ')
for index in g:
	print(index)

print('Initial assignments: ', k)
print()

print('Final Assignments: ', DBM(g, k, number_of_users, number_of_arms))
print()
'''














